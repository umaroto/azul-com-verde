<?php
Router::connect('/', array('controller' => 'pages', 'action' => 'index'));
Router::connect('/admin', array('controller' => 'parceiros', 'action' => 'index', 'admin' => true));

Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

Router::parseExtensions('json');

CakePlugin::routes();

require CAKE . 'Config' . DS . 'routes.php';