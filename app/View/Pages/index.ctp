<main role="main">
	<section class="jumbotron text-center">
		<div class="container">
			<h1>Azul com Verde</h1>
			<p class="lead text-muted white">Somos uma Administradora de Meios de Hospedagem com experiência de 7 anos no mercado brasileiro.<br>A única administradora com gestão 100% online, programa de trainee próprio, e com experiência em quartos, camas e horas;</p>
		</div>
	</section>

	<section id="sobre">
		<div class="container">
			<div class="row">
				<div class="col-xl-12">
					<h1>Sobre nós</h1>

					<p class="text-muted">Somos uma equipe de profissionais que possuem mais de 10 anos de experiência no mercado empresarial no Brasil, sendo que todos já possuem cases de sucesso em diversas áreas da hotelaria de pequeno e médio porte. Nossa empresa também conta com um conselho administrativo, formado por executivos e empresários com mais de 20 anos de experiência em empresas de grande porte.<br><br>Com todo esse know-how nossa equipe está preparada para auxiliar e administrar meios de hospedagem de todo o Brasil, com ferramentas, procedimentos, indicadores, e mentorias em todas as áreas necessárias para que o seu Hotel / Pousada / Hostel tenha sucesso no desenvolvimento dos seus negócios. Nossa equipe assume o compromisso com os resultados esperados pelos investidores e proprietários, que receberam relatórios mensais.</p>

					<h2>Propósito</h2>
					<p class="text-muted">Nosso propósito é profissionalizar a gestão da hoteleira independente no Brasil, através de novas tecnologias acessíveis.</p>
					
					<h2>Missão</h2>
					<p class="text-muted">Mudar a forma das pessoas se hospedarem, incluindo experiências de interatividade humana nos meios de hospedagens.</p>

					<h2>Visão</h2>
					<p class="text-muted">Sermos Transparentes com nossa gestão;<br>Sermos Inovadores com nossas soluções;<br>Sermos Éticos com nossos stakeholders;<br>Sermos Audaciosos na mudança do Brasil;</p>

					<h2>Valores</h2>
					<p class="text-muted">Tornar os meios de hospedagens independentes fortes para competirem com grandes redes.</p>
				</div>
			</div>
		</div>
	</section>

	<!-- paralax -->
	<section id="parallax">
		<div class="container-fluid">
			<div class="row">
				<div class="parallax">
					<div class="container">
						<div class="row">
							Para se ter sucesso, é necessário amar de verdade o que se faz. Caso contrário, levando em conta apenas o lado racional, você simplesmente desiste. É o que acontece com a maioria das pessoas.<br>
							<div class="assinatura">Steve Jobs</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="servicos">
		<div class="container">
			<div class="row">
				<div class="col-xl-12">
					<h1>O Que Fazemos</h1>
					<p class="text-muted">
						Somos especialistas em fazer o seu equipamento hoteleiro administrar melhor seus recursos financeiros, e intelectuais com o propósito de alcançar resultados mais eficientes em prol do seu Hotel/Pousada/Hostel.
					</p>
					<p class="text-muted">
						Temos equipe com expertise em fazer os Hotéis e Pousadas familiares e agirem como uma empresa profissional, implantando indicadores, procedimentos, e ferramentas.
					</p>

					<p class="text-muted">
						Temos know-how para reformular uma cultura organizacional e implantação de novos hábitos e costumes em toda a equipe de trabalho. Também melhoramos a comunicação interna da empresa gerando resultados mais satisfatórios com menos esforços.
					</p>

					<p class="text-muted">
						Somos capazes de criar metodologias e procedimentos personalizados para o desenvolvimento do seu meio de hospedagem, e treinamos a liderança a dar continuidade no trabalho de formação de equipe durante o período mínimo de 2 anos.
					</p>
					<p class="text-muted">
						Todo nosso trabalho é planejado e personalizado para o seu meio de hospedagem, pela nossa própria equipe estratégica.
					</p>

					<h3>Planejamento Estratégico</h3>
					<p class="text-muted">
						Realizamos pesquisas de mercado, adaptamos o modelo de negócio, e analisamos as influências do setor para os próximos anos, finalizando um estudo que guiará as tomadas de decisões da empresa pelos próximos anos de operação. Esse trabalho é fundamental para a implantação de qualquer equipamento hoteleiro, e esse estudo leva de 1 a 3 meses para ser concluído.
					</p>

					<h3>Implantação de Meios de Hospedagens</h3>
					<p class="text-muted">
						Somos especialistas na implantação de novos meios de hospedagens, trabalhando com metodologia bootstrap, que vai evoluindo nos primeiros meses da empresa.<br>
						Criamos equipes do zero, com processos de recrutamento e seleção, treinamento, e desenvolvimento de habilidades, conseguindo operar com o menor custo de capital de giro do mercado, através do nossos acompanhamento semanal.<br>Conseguimos eliminar as dores de 90% dos empresários brasileiros que é a mão de obra na hotelaria, através de uma equipe experiente no treinamento e desenvolvimento humano.</p>

					<h3>Administração de Hotéis, Pousadas e Hostels</h3>
					<p class="text-muted">
						Fazemos uma administração eficiente e rentável para todas as empresas que trabalhamos, oferecendo uma rentabilidade média de 3,5% e margem de contribuição acima de 25%. Trabalhamos com indicadores e novas tecnologias que nos permitem monitorar todas as áreas da empresa nos mínimos detalhes. Toda administração é transparente, através de relatórios mensais, auditorias externas trimestrais, e softwares online que permitem o monitoramento em tempo real.
					</p>
					<p class="text-muted">
						<strong>Operacional</strong><br>
						Na parte operacional trabalhamos com relatórios semanais de indicadores como satisfação do cliente, problemas operacionais, prejuízos operacionais, entre outros, com o objetivo de melhorar a experiência do cliente dentro do Hotel/Pousada/Hostel.
					</p>
					<p class="text-muted">
						<strong>Administrativo</strong><br>
						Na área administrativa trabalhamos com indicadores semanais de feedbacks de equipe, controle de desempenho, nível de rentabilidade intelectual, controle de custos gerais, controle de custos individualizados, metas alcançadas, entre outras, focando sempre da satisfação da equipe e controle de custos.
					</p>
					<p class="text-muted">
						<strong>Comercial</strong><br>
						Na parte comercial contamos com equipes de vendas: dedicada ao cliente final, dedicada a clientes corporativos, dedicada a parcerias.
					</p>

					<p class="text-muted">
						Temos um departamento inteiro especializado em Revenue Management (RM), que acompanha resultados através de relatórios diários, fazendo a gestão de tarifas diariamente do seu Hotel / Pousada / Hostel. Através de softwares e tecnologias avançadas eles controlam as vendas e cadastros em todas OTA’s e seu site próprio, buscando sempre o maior lucro para o seu meio de hospedagem.
					</p> 
					
					<p class="text-muted">
						Ainda contamos com uma equipe de tecnologia direcionada as vendas, que criam oferecem diversas ferramentas que proporcionam uma melhor performance das suas vendas diretas, através de motor de reservas personalizado, programa de fidelidade automatizado, guia turístico online, tour virtual de reservas, chat bot, dashboard de indicadores online, e website próprio para Hotéis, Pousadas, e Hostels.
					</p>

					<p class="text-muted">
						E a nossa Central de reservas atende com PABX na nuvem, possibilitande o  direcionamento ao seu estabelecimento, com gravação de ligações, e relatórios do volume de ligações.
					</p>
				</div>
			</div>
		</div>
	</section>

	<section id="clientes">
		<div class="container">
			<div class="row">
				<div class="col-xl-12">
					<h1>Clientes</h1>
					<p class="text-muted">Administramos Hotéis, Pousadas e Hostels, conseguindo aumentar as vendas e diminuir os custos dos meios de hospedagens.</p>
				</div>
				<div class="col-xl-12 mt-2 owl-carousel carousel-parceiros">
					<?php
					foreach ($clientes as $key => $cliente) {
						?>
						<div class="item p1">
							<div class="shadow-sm">
								<?php echo $this->Html->image('clientes/' . $cliente['Cliente']['arquivo']);?>
							</div>
						</div>
						<?php
					}
					?>
				</div>
			</div>
		</div>
	</section>

	<!-- paralax -->
	<section id="parallax">
		<div class="container-fluid">
			<div class="row">
				<div class="parallax">
					<div class="container">
						<div class="row">
							Pessoas trabalham melhor quando sabem qual é o objetivo e o porquê. É importante que as pessoas tenham vontade de ir trabalhar de manhã e curtir trabalhar.<br>
							<div class="assinatura">Elon Musk</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="equipe">
		<div class="container">
			<div class="row">
				<div class="col-xl-12">
					<h1 class="mb-4">Equipe</h1>
				</div>
				<div class="col-xl-12">
					<div class="row">
						<div class="col-md-3">
							<div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
								<div class="col p-4 d-flex flex-column position-static">
									<strong class="d-inline-block mb-2 verde">João Luiz de Lima Junior<br><span class="text-muted">CEO</span></strong>
									<p class="card-text mb-auto">Empreendedor e Fundador do The Hostel e Grupo ACV. Atuando no ramo de hotelaria e turismo há mais de 12 anos.</p>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
								<div class="col p-4 d-flex flex-column position-static">
									<strong class="d-inline-block mb-2 verde">Aline Beltrame<br><span class="text-muted">Diretora de Pricing</span></strong>
									<p class="card-text mb-auto">Experiência de 12 anos em precificação de produtos e serviços. Empreendedora e Consultora na área de turismo e hotelaria.</p>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
								<div class="col p-4 d-flex flex-column position-static">
									<strong class="d-inline-block mb-2 verde">Anderson Fernandes<br><span class="text-muted">Analista Financeiro</span></strong>
									<p class="card-text mb-auto">Experiência em consultorias e treinamentos financeiros, análises de DRE's e Capital de Giro, e planejamento estratégico de finanças.</p>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
								<div class="col p-4 d-flex flex-column position-static">
									<strong class="d-inline-block mb-2 verde">Eliana Santos<br><span class="text-muted">Diretora de RH</span></strong>
									<p class="card-text mb-auto">Atua na área de recrutamento e seleção, treinamentos, e desenvolvimento de pessoas na área hoteleira. Experiência de 10 anos no segmento.</p>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
								<div class="col p-4 d-flex flex-column position-static">
									<strong class="d-inline-block mb-2 verde">Marília Cremaschi<br><span class="text-muted">CEO do The Hostel Salvador</span></strong>
									<p class="card-text mb-auto">Empreendedora especialista no segmento hosteleiro, com experiência na liderança de equipes e cultura organizacional.</p>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
								<div class="col p-4 d-flex flex-column position-static">
									<strong class="d-inline-block mb-2 verde">Renato Castanheiro<br><span class="text-muted">Diretor de Novas Tecnologia</span></strong>
									<p class="card-text mb-auto">Experiência de 15 anos em desenvolvimento de sistemas para a internet. Empreendedor na área de tecnologia.</p>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
								<div class="col p-4 d-flex flex-column position-static">
									<strong class="d-inline-block mb-2 verde">Lucas Lourenço<br><span class="text-muted">Diretor Comercial</span></strong>
									<p class="card-text mb-auto">Empreendedor com especialização em em vendas e planejamento estratégico</p>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
								<div class="col p-4 d-flex flex-column position-static">
									<strong class="d-inline-block mb-2 verde">Vinicius Eiras<br><span class="text-muted">Diretor de Operações</span></strong>
									<p class="card-text mb-auto">Mais de 10 anos de experiência na operação hoteleiro e pesquisa de mercado. Especialista em consultorias e implantações.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="depoimentos">
		<!-- <div class="container">
			<div class="row">
				<div class="col-xl-12">
					<h1>Depoimentos</h1>
					<p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam lobortis at mauris pellentesque fermentum. Mauris id lobortis nisl. Ut pellentesque dignissim quam, eget scelerisque justo dapibus eget.</p>
				</div>
				<div class="col-xl-12">
					<div class="testimonials owl-carousel" id="carousel-depoimentos">
						<?php
						foreach ($depoimentos as $key => $depoimento) {
							?>
							<div class="testimonial-item">
								<p>
									<?php echo $this->Html->image('depoimentos/' . $depoimento['Depoimento']['arquivo']);?>
									"<?php echo nl2br($depoimento['Depoimento']['texto']);?>"
								</p>
								<div class="testimonials-arrow"></div>
								<div class="author">
									<a href="javascript:void(0);"><span class="color"><?php echo $depoimento['Depoimento']['nome'];?></span></a>, <?php echo $depoimento['Depoimento']['cargo'];?>, <?php echo $depoimento['Depoimento']['empresa'];?>
								</div>
							</div>
							<?php
						}
						?>
					</div>
				</div>
			</div>
		</div>-->
	</section>
	

	<section id="parceiros">
		<div class="container">
			<div class="row">
				<div class="col-xl-12">
					<h1>Parceiros</h1>
					<p class="text-muted">Abaixo alguns parceiros que nos dão suporte para entregarmos a melhor administração de meios de hospedagens do Brasil.</p>
				</div>
				<div class="col-xl-12 mt-2 owl-carousel carousel-parceiros">
					<?php
					foreach ($parceiros as $key => $parceiro) {
						?>
						<div class="item">
							<?php echo $this->Html->image('parceiros/' . $parceiro['Parceiro']['arquivo']);?>
						</div>
						<?php
					}
					?>
				</div>
			</div>
		</div>
	</section>

	<section id="contato">
		<div class="container">
			<div class="row">
				<div class="col-xl-12">
					<h1>Contato</h1>
					<p class="text-muted mb-4">
						Caso ainda tenha dúvidas sobre os nossos serviços e nossa administração, nos envie um e-mail e um dos nossos consultores irão entrar em contato para fazer um call ou marcar uma visita de apresentação dos serviços.
					</p>
					<div class="clearfix"></div>
					<!-- Start Google Map -->
					<iframe src="https://maps.google.com/maps?q=Rua Professor Gustavo Pires de Andrade, 425 - Vila Prudente São Paulo, SP 03140-010&output=embed" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
				<!-- <div class="col-xl-4 mt-4" id="contato-info">
					<div class="row">
						<h4 class="mb-2">Contatos</h3>
						<p class="info text-muted">(00) 00000-0000 <i class="fas fa-phone"></i></p>
						<p class="info text-muted">teste@gmail.com <i class="fas fa-envelope"></i></p>
						
						<h4 class="mt-2 mb-2">Redes Sociais</h3>
						<div class="share-social">
							<a href="#" target="_blank"><i class="fab fa-facebook"></i></a>
							<a href="#" target="_blank"><i class="fab fa-twitter"></i></a>
							<a href="#" target="_blank"><i class="fab fa-google-plus"></i></a>
						</div>
					</div>
				</div> -->
			</div>
		</div>
	</section>

	<div id="contato-suspenso">
			<div class="header">Solicite uma apresentação</div>
			<div class="contato-body">
				<div class="row">
					<div class="col-lg-12 mt-2">
						<form action="<?php echo Router::url('/', array('full' => true));?>" method="post" id="contact-form">
							<div class="row" id="fields">								
								<div class="form-box col-lg-12 mb-3">
									<label>Nome <small>*</small></label>
									<input type="text" id="nomeForm" name="contato_nome" class="form-control">
								</div>
								<div class="form-box col-lg-12 mb-3">
									<label>Email <small>*</small></label>
									<input type="text" id="emailForm" name="contato_email" class="form-control">
								</div>
								<div class="form-box col-lg-12 mb-3">
									<label>Assunto <small>*</small></label>
									<input type="text" id="assuntoForm" name="contato_assunto" class="form-control">
								</div>

								<div class="form-box col-lg-12 mb-3">
									<label>Mensagem <small>*</small></label>
									<textarea id="mensagemForm" name="contato_mensagem" rows="7" class="form-control"></textarea>
								</div>
								<!-- End Box -->
								<div class="clearfix"></div>

								<div class="form-box col-lg-12 mb-3">
									<input type="submit" value="Enviar" class="btn submit btn-dark">	
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
</main>

<footer>
	<div class="container">
		<p class="float-right">
			<a href="#body_document" class="scrollLink">
				<?php echo $this->Html->image('arrow.png');?>
			</a>
		</p>
		<div class="row">
			<div class="col-lg-5 mt-3 mb-3">
				<h4 class="text-left">Newsletter</h4>
				<?php
				echo $this->Form->create('Newsletter', array('url' => array('controller' => 'newsletters', 'action' => 'add'), 'name' => 'form_newsletter', 'id' => 'form_newsletter', 'role' => 'form', 'class' => 'form-inline'));
				?>
					<div class="form-group">
						<input type="email" class="form-control mb-2" id="NewsletterEmail" name="data[Newsletter][email]" placeholder="Seu email">
						<!-- <small class="form-text text-muted">Nunca vamos compartilhar seu email, com ninguém.</small> -->
					</div>
					<button type="submit" class="btn btn-inverse mb-2 ml-2">Enviar</button>							
				</form>
				<small class="form-text text-muted ml-1 white">Nunca vamos compartilhar seu email, com ninguém.</small>
			</div>

			<div class="col-lg-3 mt-5 mb-3 offset-lg-4 text-right">
				<?php echo $this->Html->image('logo_footer.png');?>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="desenvolvido">
			Desenvolvido por <a href="http://booles.com.br/br/ga/azulcomverde" target="_blank"><?php echo $this->Html->Image('booles.png');?> Booles</a>
		</div>
	</div>
</footer>