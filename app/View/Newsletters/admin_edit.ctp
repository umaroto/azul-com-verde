<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title"><?php echo 'ID: ' . $this->request->data['Newsletter']['id'] .  ' - ' . $this->request->data['Newsletter']['email'];?></h4>
</div>
<div class="modal-body">
	<div class="hostels">
		<?php
		echo $this->Form->create('Newsletter', array('url' => array('controller' => 'newsletters', 'action' => 'add'), 'class' => 'form-horizontal ', 'role' => 'form'));
			echo $this->Form->input('Newsletter.id');
			?>
			
			<div class="form-group">
				<label for="NewsletterEmail" class="col-sm-1 control-label">Email</label>
				<div class="col-sm-11">
					<?php
					echo $this->Form->input('Newsletter.email', array('class' => 'form-control', 'label' => false, 'div' => false));
					?>
				</div>
			</div>

			<br class="clear" />
			<button type="submit" class="btn btn-default waves-effect waves-light btn-md">Salvar</button>
		</form>
	</div>
</div>