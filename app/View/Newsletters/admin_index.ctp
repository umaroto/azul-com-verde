<div class="content">
	<div class="container">

		<?php echo $this->Session->flash(); ?>

		<!-- Modal -->
		<div id="myModal" class="modal fade" role="dialog">
			<div class="modal-dialog modal-lg">
				<div class="modal-content"></div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<h4 class="page-title"><?php echo ucfirst($this->request->controller);?></h4>
				<ol class="breadcrumb">
					<li class="active">
						Listagem
					</li>
				</ol>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<div class="card-box">
					<div class="table-rep-plugin">
						<div class="table-wrapper">
							<div class="btn-toolbar">
								<div class="btn-group focus-btn-group">
									<a class="btn btn-default" href="<?php echo Router::url('/', array('full' => true));?>admin/newsletters/exporta">Baixar Newsletter <i class="fa fa-plus"></i></a>
								</div>

								<div class="input-group pull-right col-sm-3">
									<input type="text"  class="form-control" id="search" placeholder="Pesquisar">
									<span class="input-group-btn">
										<input type="button" id="SearchButton" class="btn btn-default" value="Buscar" />
									</span>
									<?php
									echo $this->Form->create('Newsletter', array('class' => 'hidden', 'role' => 'form'));
										echo $this->Form->input('Newsletter.id');
										echo $this->Form->input('Newsletter.email');
									echo $this->Form->End('');
									?>
								</div>
							</div>
						</div>
					</div>

					<table class="table table-striped m-t-40">
						<thead>
							<tr>
								<th>
									<?php
									echo $this->Paginator->sort(
										'email',
										'Email',
										array('escape' => false)
									);
									?>
								</th>
								<th>
									<?php
									echo $this->Paginator->sort(
										'created',
										'Criado em',
										array('escape' => false)
									);
									?>
								</th>
								<th>Ações</th>
							</tr>
						</thead>
						<tbody>					
							<?php
							foreach ($newsletters as $k => $newsletter) :
								?>
								<tr class="newsletter" id="row_<?php echo $newsletter['Newsletter']['id'];?>">
									<td><?php echo $newsletter['Newsletter']['email'];?></td>
									<td><?php echo $this->Global->data_ptBR($newsletter['Newsletter']['created']);?></td>
									<td class="center" width="15%">
										<div class="button-list">
											<a class="btn btn-icon waves-effect waves-light btn-default edit" href="<?php echo Router::url(array('action' => 'edit', $newsletter['Newsletter']['id']));?>" data-toggle="modal" data-target="#myModal">
												<i class="fa fa-wrench"></i>
											</a>
										</div>
									</td>
								</tr>
								<?php
							endforeach;
							?>
						</tbody>
					</table>

					<?php
					echo $this->Element('admin/pagination');
					?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
echo $this->Html->scriptBlock("
	$(document).ready(function() {
		$( '#search' ).autocomplete({
			source: '".Router::url('/', array('full' => true)).'admin/'.$this->request->controller."/search',
			minLength: 3,
			select: function( event, ui ) {
				$('#NewsletterAdminIndexForm #NewsletterId').val(ui.item.id);
			}
		});

		$('#SearchButton').bind('click', function(e){
			e.preventDefault();
			$('#NewsletterAdminIndexForm #NewsletterId').val();
			$('#NewsletterAdminIndexForm #NewsletterTitulo').val($('#search').val());
			$('#NewsletterAdminIndexForm').submit();
		});

		$(document).on('hidden.bs.modal', function (e) {
			$(e.target).removeData('bs.modal');
		});
	});", array('block' => 'scriptBottom'));