<?php echo $this->Session->flash(); ?>

<form class="form-horizontal m-t-20" id="UserForm">
	<div class="form-group ">
		<div class="col-xs-12">
			<?php
			echo $this->Form->input('User.username', array('class' => 'form-control', 'label' => false, 'div' => false, 'placeholder' => 'Usuário'));
			?>
		</div>
	</div>

	<div class="form-group">
		<div class="col-xs-12">
			<?php
			echo $this->Form->input('User.password', array('class' => 'form-control', 'label' => false, 'div' => false, 'placeholder' => 'Senha'));
			?>
		</div>
	</div>
	
	<div class="form-group text-center m-t-40">
		<div class="col-xs-12">
			<button class="btn btn-pink btn-block text-uppercase waves-effect waves-light" type="submit" id="UserSubmit">Log In</button>
		</div>
	</div>
</form>

<?php
echo $this->Html->scriptBlock("
	$(document).ready(function() {
		$('#UserSubmit').bind('click', function(e){
			e.preventDefault();
			$.post(
				'".Router::url('/')."admin/users/logged', {
					username: $('#UserUsername').val(),
					password: $('#UserPassword').val()
				}
			).done(function( data ) {
				var obj = jQuery.parseJSON( data );
				$.Notification.notify(obj.response,'top left', obj.title, obj.message);

				if(obj.url){
					setTimeout(function(){
						window.location = obj.url
					}, 2000);
				}
			});
		});
	} );", array('block' => 'scriptBottom')); 