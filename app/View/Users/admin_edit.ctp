<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title"><?php echo 'ID: ' . $this->request->data['User']['id'] .  ' - ' . $this->request->data['User']['name'];?></h4>
</div>
<div class="modal-body">
	<div class="users">
		<?php
		echo $this->Form->create('User', array('url' => array('controller' => 'users', 'action' => 'add'), 'class' => 'form-horizontal ', 'role' => 'form'));
			echo $this->Form->input('User.id');
			?>
			<div class="form-group">
				<label for="UserNome" class="col-sm-1 control-label">Nome</label>
				<div class="col-sm-11">
					<?php
					echo $this->Form->input('User.name', array('class' => 'form-control', 'label' => false, 'div' => false));
					?>
				</div>
			</div>

			<div class="form-group">
				<label for="UserEmail" class="col-sm-1 control-label">Email</label>
				<div class="col-sm-11">
					<?php
					echo $this->Form->input('User.email', array('class' => 'form-control', 'label' => false, 'div' => false));
					?>
				</div>
			</div>

			<div class="form-group">
				<label for="UserUsername" class="col-sm-1 control-label">Usuário</label>
				<div class="col-sm-11">
					<?php
					echo $this->Form->input('User.username', array('class' => 'form-control', 'label' => false, 'div' => false));
					?>
				</div>
			</div>


			<div class="form-group">
				<label for="UserPassword" class="col-sm-1 control-label">Senha</label>
				<div class="col-sm-11">
					<?php
					echo $this->Form->input('User.password', array('class' => 'form-control', 'label' => false, 'div' => false));
					?>
				</div>
			</div>

			<div class="form-group">
				<label for="UserNivel" class="col-sm-1 control-label">Nível</label>
				<div class="col-sm-11">
					<?php
					$nivel = $this->Session->read('Auth.User.role');

					if($nivel == 0){
						?>
						<div class="radio radio-custom m-l-20">
							<?php $checked = isset($this->request->data['User']['role']) && $this->request->data['User']['role'] == 0 ? ' checked="checked"' : '';?>
							<input type="radio" id="UserNivel0" value="0" name="data[User][role]"<?php echo $checked;?>>
							<label for="UserNivel0"> Super </label>
						</div>
						<?php
					}

					if($nivel <= 1){
						?>
						<div class="radio radio-custom m-l-20">
							<?php $checked = isset($this->request->data['User']['role']) && $this->request->data['User']['role'] == 1 ? ' checked="checked"' : '';?>
							<input type="radio" id="UserNivel1" value="1" name="data[User][role]"<?php echo $checked;?>>
							<label for="UserNivel1"> Admin </label>
						</div>

						<div class="radio radio-custom m-l-20">
							<?php $checked = !isset($this->request->data['User']['role']) || (isset($this->request->data['User']['role']) && $this->request->data['User']['role'] == 2) ? ' checked="checked"' : '';?>
							<input type="radio" id="UserNivel2" value="2" name="data[User][role]"<?php echo $checked;?>>
							<label for="UserNivel2"> Usuário </label>
						</div>
						<?php
					}
					?>
				</div>
			</div>

			<div class="form-group">
				<label for="UserActive" class="col-sm-1 control-label">Ativo</label>
				<div class="col-sm-11">
					<div class="radio radio-custom m-l-20">
						<?php $checked = !isset($this->request->data['User']['active']) || (isset($this->request->data['User']['active']) && $this->request->data['User']['active'] == 'S') ? ' checked="checked"' : '';?>
						<input type="radio" id="UserActiveS" value="S" name="data[User][active]"<?php echo $checked;?>>
						<label for="UserActiveS"> Ativo </label>
					</div>
					<div class="radio radio-custom m-l-20">
						<?php $checked = isset($this->request->data['User']['active']) && $this->request->data['User']['active'] == 'N' ? ' checked="checked"' : '';?>
						<input type="radio" id="UserActiveN" value="N" name="data[User][active]"<?php echo $checked;?>>
						<label for="UserActiveN"> Inativo </label>
					</div>
				</div>
			</div>

			<br class="clear" />
			<button type="submit" class="btn btn-default waves-effect waves-light btn-md">Salvar</button>
		</form>
	</div>
</div>