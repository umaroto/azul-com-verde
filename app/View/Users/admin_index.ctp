<div class="content">
	<div class="container">

		<?php echo $this->Session->flash(); ?>

		<!-- Modal -->
		<div id="myModal" class="modal fade" role="dialog">
			<div class="modal-dialog modal-lg">
				<div class="modal-content"></div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<h4 class="page-title"><?php echo ucfirst($this->request->controller);?></h4>
				<ol class="breadcrumb">
					<li class="active">
						Listagem
					</li>
				</ol>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<div class="card-box">
					<div class="table-rep-plugin">
						<div class="table-wrapper">
							<div class="btn-toolbar">
								<div class="btn-group focus-btn-group">
									<a class="btn btn-default">Inserir Novo <i class="fa fa-plus"></i></a>
								</div>

								<div class="input-group pull-right col-sm-3">
									<input type="text"  class="form-control" id="search" placeholder="Pesquisar">
									<span class="input-group-btn">
										<input type="button" id="SearchButton" class="btn btn-default" value="Buscar" />
									</span>
									<?php
									echo $this->Form->create('User', array('class' => 'hidden', 'role' => 'form'));
										echo $this->Form->input('User.id');
										echo $this->Form->input('User.name');
									echo $this->Form->End('');
									?>
								</div>
							</div>
						</div>
					</div>

					<div class="new">
						<div class="m-t-40">
							<?php
							echo $this->Form->create('UserAdd', array('url' => array('controller' => 'users', 'action' => 'add'), 'class' => 'form-inline ', 'role' => 'form'));
								echo $this->Form->input('User.name', array('class' => 'form-control m-r-5', 'label' => array('class' => 'form-label', 'text' => 'Nome'), 'div' => 'form-group'));
								echo $this->Form->input('User.email', array('class' => 'form-control m-r-5', 'label' => array('class' => 'form-label', 'text' => 'Email'), 'div' => 'form-group'));
								echo $this->Form->input('User.username', array('class' => 'form-control m-r-5', 'label' => array('class' => 'form-label', 'text' => 'Usuário'), 'div' => 'form-group'));
								echo $this->Form->input('User.password', array('class' => 'form-control m-r-5', 'label' => array('class' => 'form-label', 'text' => 'Senha'), 'div' => 'form-group'));
								?>

								<div class="form-group m-l-10">
									<label for="UserNivel" class="form-label">Nível</label>

									<?php
									$nivel = $this->Session->read('Auth.User.role');

									if($nivel == 0){
										?>
										<div class="radio radio-custom radio-inline">
											<?php $checked = isset($this->request->data['User']['role']) && $this->request->data['User']['role'] == 0 ? ' checked="checked"' : '';?>
											<input type="radio" id="UserRole0" value="0" name="data[User][role]"<?php echo $checked;?>>
											<label for="UserRole0"> Super </label>
										</div>
										<?php
									}

									if($nivel <= 1){
										?>
										<div class="radio radio-custom radio-inline">
											<?php $checked = isset($this->request->data['User']['role']) && $this->request->data['User']['role'] == 1 ? ' checked="checked"' : '';?>
											<input type="radio" id="UserRole1" value="1" name="data[User][role]"<?php echo $checked;?>>
											<label for="UserRole1"> Admin </label>
										</div>

										<div class="radio radio-custom radio-inline">
											<?php $checked = !isset($this->request->data['User']['role']) || (isset($this->request->data['User']['role']) && $this->request->data['User']['role'] == 2) ? ' checked="checked"' : '';?>
											<input type="radio" id="UserRole2" value="2" name="data[User][role]"<?php echo $checked;?>>
											<label for="UserRole2"> Usuário </label>
										</div>
										<?php
									}
									?>
								</div>

								<div class="form-group m-l-15">
									<label for="UserActive" class="form-label">Ativo</label>

									<div class="radio radio-custom radio-inline">
										<?php $checked = !isset($this->request->data['User']['active']) || (isset($this->request->data['User']['active']) && $this->request->data['User']['active'] == 'S') ? ' checked="checked"' : '';?>
										<input type="radio" id="UserActiveS" value="S" name="data[User][active]"<?php echo $checked;?>>
										<label for="UserActiveS"> Ativo </label>
									</div>
									<div class="radio radio-custom radio-inline">
										<?php $checked = isset($this->request->data['User']['active']) && $this->request->data['User']['active'] == 'N' ? ' checked="checked"' : '';?>
										<input type="radio" id="UserActiveN" value="N" name="data[User][active]"<?php echo $checked;?>>
										<label for="UserActiveN"> Inativo </label>
									</div>
								</div>

								<br class="clear" />
								<button type="submit" class="btn btn-default waves-effect waves-light btn-md">Salvar</button>
							</form>
						</div>
					</div>

					<table class="table table-striped m-t-40">
						<thead>
							<tr>
								<th>
									<?php
									echo $this->Paginator->sort(
										'name',
										'Nome',
										array('escape' => false)
									);
									?>
								</th>
								<th>
									<?php
									echo $this->Paginator->sort(
										'username',
										'Usuário',
										array('escape' => false)
									);
									?>
								</th>
								<th>
									<?php
									echo $this->Paginator->sort(
										'role',
										'Nível',
										array('escape' => false)
									);
									?>
								</th>
								<th>
									<?php
									echo $this->Paginator->sort(
										'active',
										'Ativo',
										array('escape' => false)
									);
									?>
								</th>
								<th>Ações</th>
							</tr>
						</thead>
						<tbody>					
							<?php
							foreach ($users as $k => $user) :
								?>
								<tr class="user" id="row_<?php echo $user['User']['id'];?>">
									<td><?php echo $user['User']['name'];?></td>
									<td><?php echo $user['User']['username'];?></td>
									<td><?php echo $this->Global->formataNivel($user['User']['role']);?></td>
									<td>
										<?php $class = $user['User']['active'] == 'S' ? 'default' : 'danger';?>
										<a class="btn btn-small btn-<?php echo $class;?>"><?php echo $user['User']['active'];?></a>
									</td>
									<td class="center" width="15%">
										<div class="button-list">
											<a class="btn btn-icon waves-effect waves-light btn-default edit" href="<?php echo Router::url(array('action' => 'edit', $user['User']['id']));?>" data-toggle="modal" data-target="#myModal">
												<i class="fa fa-wrench"></i>
											</a>
											<a href="javascript:void(0);" id="delete_<?php echo $user['User']['id'];?>" class="btn btn-icon waves-effect waves-light btn-danger delete">
												<i class="fa fa-remove"></i>
											</a>
										</div>
									</td>
								</tr>
								<?php
							endforeach;
							?>
						</tbody>
					</table>

					<?php
					echo $this->Element('admin/pagination');
					?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
echo $this->Html->script(array('admin/jquery.maskedinput.min', 'admin/ui.datepicker-pt-BR', '../plugins/notifyjs/dist/notify.min', '../plugins/notifications/notify-metro'), array('block' => 'scriptBottom'));
echo $this->Html->scriptBlock("
	$(document).ready(function() {
		$( '#search' ).autocomplete({
			source: '".Router::url('/', array('full' => true)).'admin/'.$this->request->controller."/search',
			minLength: 3,
			select: function( event, ui ) {
				$('#UserAdminIndexForm #UserId').val(ui.item.id);
				$('#UserAdminIndexForm').submit();
			}
		});

		$('#SearchButton').bind('click', function(e){
			e.preventDefault();
			$('#UserAdminIndexForm #UserId').val();
			$('#UserAdminIndexForm #UserName').val($('#search').val());
			$('#UserAdminIndexForm').submit();
		})

		$('.focus-btn-group').bind('click', function(e){
			e.preventDefault();
			$('.new').slideDown();
		});

		$(document).on('click', '.user .delete', function(){
			if (confirm('Tem certeza que você quer excluir este registro?')) {
				$.post(
					'".Router::url('/', array('full' => true))."admin/users/delete',
					{
						id: $(this).attr('id')
					}
				).done(function( data ) {
					if(data > 0){
						$.Notification.notify('success','top left', 'Sucesso', 'Registro excluído com sucesso.');						
						$('tr#row_'+data).removeClass().addClass('bounceOutLeft animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
							$(this).remove();
						});
					} else {
						$.Notification.notify('error','top left', 'Erro', 'Erro ao excluir registro.');
					}
				});
			}
		});

		$(document).on('hidden.bs.modal', function (e) {
			$(e.target).removeData('bs.modal');
		});
	});", array('block' => 'scriptBottom'));