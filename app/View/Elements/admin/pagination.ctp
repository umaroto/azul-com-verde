<div class="row m-t-40">
	<div class="col-sm-6 pagination">
		<?php echo $this->Paginator->counter('Página {:page} de {:pages}, mostrando {:current} registros de um total de {:count}');?>
	</div>

	<div class="col-sm-6">
		<ul class="pagination pull-right">
			<?php
			echo $this->Paginator->numbers(array(
				'modulus' => '8',
				'first' => 'Primeira',
				'last' => ' Última',
				'tag' => 'li',
				'class' => 'paginate_button',
				'currentClass' => 'active',
				'currentTag' => 'a',
				'separator' => null
			));
			?>
		</ul>
	</div>
</div>