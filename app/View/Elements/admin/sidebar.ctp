<div class="left side-menu">
	<div class="sidebar-inner slimscrollleft">
		<!--- Divider -->
		<div id="sidebar-menu">
			<div class="menu_separador"></div>
			<ul>
				<li>
					<?php
					$class = $this->request->controller == 'parceiros' ? ' active' : '';
					echo $this->Html->link(
						'<i class="ti-pencil-alt"></i><span> Parceiros </span>',
						Router::url('/', array('full' => true)) . 'admin/parceiros',
						array('class' => 'waves-effect' . $class, 'escape' => false)
					);
					?>
				</li>
				<li>
					<?php
					$class = $this->request->controller == 'clientes' ? ' active' : '';
					echo $this->Html->link(
						'<i class="ti-pencil-alt"></i><span> Clientes </span>',
						Router::url('/', array('full' => true)) . 'admin/clientes',
						array('class' => 'waves-effect' . $class, 'escape' => false)
					);
					?>
				</li>
				<li>
					<?php
					$class = $this->request->controller == 'newsletters' ? ' active' : '';
					echo $this->Html->link(
						'<i class="ti-pencil-alt"></i><span> Newsletter </span>',
						Router::url('/', array('full' => true)) . 'admin/newsletters',
						array('class' => 'waves-effect' . $class, 'escape' => false)
					);
					?>
				</li>
				<li>
					<?php
					$class = $this->request->controller == 'depoimentos' ? ' active' : '';
					echo $this->Html->link(
						'<i class="ti-pencil-alt"></i><span> Depoimentos </span>',
						Router::url('/', array('full' => true)) . 'admin/depoimentos',
						array('class' => 'waves-effect' . $class, 'escape' => false)
					);
					?>
				</li>
				<li>
					<?php
					$class = $this->request->controller == 'users' ? ' active' : '';
					echo $this->Html->link(
						'<i class="ti-user"></i><span> Usuários </span>',
						Router::url('/', array('full' => true)) . 'admin/users',
						array('class' => 'waves-effect' . $class, 'escape' => false)
					);
					?>
				</li>
			</ul>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>