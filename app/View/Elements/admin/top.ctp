<!-- Top Bar Start -->
<div class="topbar">

	<!-- LOGO -->
	<div class="topbar-left">
		
	</div>

	<!-- Button mobile view to collapse sidebar menu -->
	<div class="navbar navbar-default" role="navigation">
		<div class="container">
			<div class="">
				<span style="display: inline-block;padding: 20px;color: #ffffff;"></span>
				<ul class="nav navbar-nav navbar-right pull-right">
					<li class="dropdown">
						<a href="" class="dropdown-toggle profile" data-toggle="dropdown" aria-expanded="true">
							<?php
							echo $this->Html->Image('admin/avatar-1.jpg', array('img-circle'));?>
						</a>
						<ul class="dropdown-menu">
							<li><a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'logout'));?>"><i class="ti-power-off m-r-5"></i> Logout</a></li>
						</ul>
					</li>
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</div>
</div>
<!-- Top Bar End -->