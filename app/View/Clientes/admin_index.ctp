<div class="content">
	<div class="container">

		<?php echo $this->Session->flash(); ?>

		<!-- Modal -->
		<div id="myModal" class="modal fade" role="dialog">
			<div class="modal-dialog modal-lg">
				<div class="modal-content"></div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<h4 class="page-title"><?php echo ucfirst($this->request->controller);?></h4>
				<ol class="breadcrumb">
					<li class="active">
						Listagem
					</li>
				</ol>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<div class="card-box">
					<div class="table-rep-plugin">
						<div class="table-wrapper">
							<div class="btn-toolbar">
								<div class="btn-group focus-btn-group">
									<a class="btn btn-default" id="new">Inserir Novo <i class="fa fa-plus"></i></a>
								</div>

								<div class="input-group pull-right col-sm-3">
									<input type="text"  class="form-control" id="search" placeholder="Pesquisar">
									<span class="input-group-btn">
										<input type="button" id="SearchButton" class="btn btn-default" value="Buscar" />
									</span>
									<?php
									echo $this->Form->create('Cliente', array('class' => 'hidden', 'role' => 'form'));
										echo $this->Form->input('Cliente.id');
										echo $this->Form->input('Cliente.titulo');
									echo $this->Form->End('');
									?>
								</div>
							</div>
						</div>
					</div>

					<div class="new">
						<div class="m-t-40">
							<?php
							echo $this->Form->create('ClienteAdd', array('type' => 'file', 'url' => array('controller' => 'clientes', 'action' => 'add'), 'class' => 'form-inline ', 'role' => 'form'));
								?>
								<br class="clear" />
								<?php
								echo $this->Form->input('Cliente.arquivo', array('type' => 'file', 'class' => 'form-control m-r-5', 'label' => array('class' => 'form-label', 'text' => 'Logo (150px x 150px)'), 'div' => 'form-group'));
								?>
								<br class="clear" />
								<?php
								echo $this->Form->input('Cliente.nome', array('placeholder' => 'Nome', 'class' => 'form-control m-r-5', 'label' => array('class' => 'form-label', 'text' => 'Nome'), 'div' => 'form-group'));
								?>
								<br class="clear" />

								<button type="submit" class="btn btn-default waves-effect waves-light btn-md">Salvar</button>
							</form>
						</div>
					</div>

					<table class="table table-striped m-t-40">
						<thead>
							<tr>
								<th>
									<?php
									echo $this->Paginator->sort(
										'nome',
										'Nome',
										array('escape' => false)
									);
									?>
								</th>
								<th>Ações</th>
							</tr>
						</thead>
						<tbody>					
							<?php
							foreach ($clientes as $k => $cliente) :
								?>
								<tr class="cliente" id="row_<?php echo $cliente['Cliente']['id'];?>">
									<td><?php echo $cliente['Cliente']['nome'];?></td>
									<td class="center" width="15%">
										<div class="button-list">
											<a href="javascript:void(0);" id="delete_<?php echo $cliente['Cliente']['id'];?>" class="btn btn-icon waves-effect waves-light btn-danger delete">
												<i class="fa fa-remove"></i>
											</a>
										</div>
									</td>
								</tr>
								<?php
							endforeach;
							?>
						</tbody>
					</table>

					<?php
					echo $this->Element('admin/pagination');
					?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
echo $this->Html->script(array('../plugins/notifyjs/dist/notify.min', '../plugins/notifications/notify-metro'), array('block' => 'scriptBottom'));
echo $this->Html->scriptBlock("
	$(document).ready(function() {
		$( '#search' ).autocomplete({
			source: '".Router::url('/', array('full' => true)).'admin/'.$this->request->controller."/search',
			minLength: 3,
			select: function( event, ui ) {
				$('#ClienteAdminIndexForm #ClienteId').val(ui.item.id);
			}
		});

		$('#SearchButton').bind('click', function(e){
			e.preventDefault();
			$('#ClienteAdminIndexForm #ClienteId').val();
			$('#ClienteAdminIndexForm #ClienteNome').val($('#search').val());
			$('#ClienteAdminIndexForm').submit();
		})

		$('#new').bind('click', function(e){
			e.preventDefault();
			$('.new').slideDown();
		});

		$(document).on('hidden.bs.modal', function (e) {
			$(e.target).removeData('bs.modal');
		});

		$(document).on('click', '.cliente .delete', function(){
			if (confirm('Tem certeza que você quer excluir este registro?')) {
				$.post(
					'".Router::url('/', array('full' => true))."admin/clientes/delete',
					{
						id: $(this).attr('id')
					}
				).done(function( data ) {
					if(data > 0){
						$.Notification.notify('success','top left', 'Sucesso', 'Registro excluído com sucesso.');						
						$('tr#row_'+data).removeClass().addClass('bounceOutLeft animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
							$(this).remove();
						});
					} else {
						$.Notification.notify('error','top left', 'Erro', 'Erro ao excluir registro.');
					}
				});
			}
		});
	});", array('block' => 'scriptBottom'));