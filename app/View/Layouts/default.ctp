<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		
		<title><?php echo $this->fetch('title'); ?></title>

		<!-- Bootstrap core CSS -->
		<link href="https://getbootstrap.com/docs/4.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&amp;display=swap" rel="stylesheet">

		<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css">

		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
		<link href="https://fonts.googleapis.com/css?family=Satisfy&display=swap" rel="stylesheet">

		<link rel="shortcut icon" href="<?php echo Router::url('/', array('full' => true));?>favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo Router::url('/', array('full' => true));?>favicon.ico" type="image/x-icon">
		
		<?php
		echo $this->Html->charset();
		echo $this->Html->css(array('style', 'owl.carousel.min'));

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
		?>
	</head>
	<body class="bg-light" id="body_document">
		<?php echo $this->Session->flash(); ?>
		<header>
			<div class="navbar navbar shadow-sm">
				<div class="container d-flex justify-content-between">
					<a href="#" class="navbar-brand d-flex align-items-center">
						<?php echo $this->Html->Image('azulcomverde.png');?>
					</a>
					<!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button> -->
					<nav id="menu" class="navigation desktop" role="navigation">
						<a class="scrollLink sobrelink active" href="#sobre">Sobre</a>
						<a class="scrollLink servicoslink" href="#servicos">Serviços</a>
						<a class="scrollLink clienteslink" href="#clientes">Clientes</a>
						<!-- <a class="scrollLink depoimentoslink" href="#depoimentos">Depoimentos</a> -->
						<a class="scrollLink equipelink" href="#equipe">Equipe</a>
						<a class="scrollLink parceiroslink" href="#parceiros">Parceiros</a>
						<a class="scrollLink contatolink" href="#contato">Contato</a>
					</nav>

					<a href="javascript:void(0);" class="mobile" id="menu_mobile">
						<?php echo $this->Html->Image('menu.png');?>
					</a>
				</div>
			</div>

			<div id="div_menu_mobile" class="navigation">
				<a class="scrollLink" href="#sobre">Sobre</a>
				<a class="scrollLink" href="#servicos">Serviços</a>
				<a class="scrollLink" href="#clientes">Clientes</a>
				<!-- <a class="scrollLink" href="#depoimentos">Depoimentos</a> -->
				<a class="scrollLink" href="#equipe">Equipe</a>
				<a class="scrollLink" href="#parceiros">Parceiros</a>
				<a class="scrollLink" href="#contato">Contato</a>
			</div>
		</header>
		<?php echo $this->fetch('content');?>

		<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
		<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js" integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk=" crossorigin="anonymous"></script>
		<script src="js/owl.carousel.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				var controle = true;

				$( window ).resize(function() {
					altura = $( window ).height();
					$('.jumbotron').height(altura);
					$('.jumbotron .container').css('padding-top', (altura - 50) / 2);
				});

				$( "a.scrollLink" ).click(function( event ) {

					controle = false;

					event.preventDefault();
					$("html, body").animate({
						scrollTop: $($(this).attr("href")).offset().top - 100
					}, 500, function() {
						controle = true;
					});

					menu = $(this);

					$("a.scrollLink").removeClass('active');
					menu.addClass('active');

					$('#div_menu_mobile').slideUp();
					$('#div_menu_mobile').removeClass('down');
				});

				$('#menu_mobile').bind('click', function(){
					if($('#div_menu_mobile').hasClass('down')){
						$('#div_menu_mobile').slideUp();
						$('#div_menu_mobile').removeClass('down');
					} else {
						$('#div_menu_mobile').slideDown();
						$('#div_menu_mobile').addClass('down');
					}
				});

				$(window).trigger('resize');
				$(window).trigger('scroll');

				$(window).scroll(function() {
					if(controle == true){
						sobre = getPosition('sobre');
						servicos = getPosition('servicos');
						clientes = getPosition('clientes');
						//depoimentos = getPosition('depoimentos');
						parceiros = getPosition('parceiros');
						equipe = getPosition('equipe');
						contato = getPosition('contato');

						if(sobre > 0 && sobre <= 150){
							$("a.scrollLink").removeClass('active');
							$('.sobrelink').addClass('active');
						}
						else
						if(servicos > 0 && servicos <= 150){
							$("a.scrollLink").removeClass('active');
							$('.servicoslink').addClass('active');
						}
						else
						if(clientes > 0 && clientes <= 150){
							$("a.scrollLink").removeClass('active');
							$('.clienteslink').addClass('active');
						}
						else
						// if(depoimentos > 0 && depoimentos <= 150){
						// 	$("a.scrollLink").removeClass('active');
						// 	$('.depoimentoslink').addClass('active');
						// }
						// else
						if(parceiros > 0 && parceiros <= 150){
							$("a.scrollLink").removeClass('active');
							$('.parceiroslink').addClass('active');
						}
						else
						if(equipe > 0 && equipe <= 150){
							$("a.scrollLink").removeClass('active');
							$('.equipelink').addClass('active');
						}
						else
						if(contato > 0 && contato <= 150){
							$("a.scrollLink").removeClass('active');
							$('.contatolink').addClass('active');
						}
					}
				});

				// $('#carousel-depoimentos').owlCarousel({
				// 	loop:true,
				// 	margin:10,
				// 	nav:true,
				// 	responsive:{
				//         0:{
				//             items:1
				//         },
				//         1000:{
				//             items:1
				//         }
				//     }
				// });

				$('.carousel-parceiros').owlCarousel({
					loop:true,
					margin:50,
					nav:false,
					autoplay:true,
				    autoplayTimeout:2000,
				    responsive:{
				        0:{
				            items:2,
				        },
				        1000:{
				            items:6,
				        }
				    }
				});

				$('#form_newsletter').bind('submit', function(){
					erro = false;

					if($('#NewsletterEmail').val() == ''){
						$('#NewsletterEmail').addClass('is-invalid');
						$('#NewsletterEmail').next('.invalid-feedback').show();
						erro = true;
					} else {
						$('#NewsletterEmail').removeClass('is-invalid');
						$('#NewsletterEmail').next('.invalid-feedback').hide();
					}

					if(erro == false){
						return true;
					} else {
						return false;
					}
				});

				$('#contact-form').bind('submit', function(){
					erro = false;

					if($('#nomeForm').val() == ''){
						$('#nomeForm').addClass('is-invalid');
						$('#nomeForm').next('.invalid-feedback').show();
						erro = true;
					} else {
						$('#nomeForm').removeClass('is-invalid');
						$('#nomeForm').next('.invalid-feedback').hide();
					}

					if($('#emailForm').val() == ''){
						$('#emailForm').addClass('is-invalid');
						$('#emailForm').next('.invalid-feedback').show();
						erro = true;
					} else {
						$('#emailForm').removeClass('is-invalid');
						$('#emailForm').next('.invalid-feedback').hide();
					}

					if($('#assuntoForm').val() == ''){
						$('#assuntoForm').addClass('is-invalid');
						$('#assuntoForm').next('.invalid-feedback').show();
						erro = true;
					} else {
						$('#assuntoForm').removeClass('is-invalid');
						$('#assuntoForm').next('.invalid-feedback').hide();
					}

					if($('#mensagemForm').val() == ''){
						$('#mensagemForm').addClass('is-invalid');
						$('#mensagemForm').next('.invalid-feedback').show();
						erro = true;
					} else {
						$('#mensagemForm').removeClass('is-invalid');
						$('#mensagemForm').next('.invalid-feedback').hide();
					}

					if(erro == false){
						return true;
					} else {
						return false;
					}
				});

				$('#contato-suspenso .header').bind('click', function(){
					$('#contato-suspenso .contato-body').slideToggle();
				});

				$('#pop a.close').bind('click', function(){
					$('#pop').fadeOut('slow', function(){
						$('#mask').fadeOut('fast');
					});
				});
			});

			function getPosition(elem){
				elem = document.getElementById(elem);
				distanceScrolled = document.body.scrollTop;
				elemRect = elem.getBoundingClientRect();
				elemViewportOffset = elemRect.top;
				totalOffset = distanceScrolled + elemViewportOffset;
				return totalOffset;
			}
		</script>

		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-138176325-2"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-138176325-2');
		</script>

	</body>
</html>