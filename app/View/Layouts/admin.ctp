<!DOCTYPE html>
<html>
	<head>
		<?php echo $this->Html->charset(); ?>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="robots" content="noindex, nofollow" />

		<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css('admin/jquery-ui');
		echo $this->Html->css('admin/bootstrap.min');
		echo $this->Html->css('admin/core');
		echo $this->Html->css('admin/components');
		echo $this->Html->css('admin/icons');
		echo $this->Html->css('admin/pages');
		echo $this->Html->css('admin/responsive');
		echo $this->Html->css('../plugins/typeahead/typeahead');
		echo $this->Html->css('../plugins/animate.less/animate.min');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
		?>

		<title><?php echo $this->fetch('title'); ?></title>
		
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<?php echo $this->Html->script(array('admin/modernizr.min'));?>

	</head>


	<body class="fixed-left">

		<div id="mask"></div>
		<div id="loader"></div>

		<div id="wrapper">

			<?php echo $this->element('admin/top');?>

			<?php echo $this->element('admin/sidebar');?>

			<div class="content-page">
				<?php echo $this->fetch('content');?>
			</div>

		</div>

		<script>
			var resizefunc = [];
		</script>

		<script src="https://code.jquery.com/jquery-1.12.3.min.js" integrity="sha256-aaODHAgvwQW1bFOGXMeX+pC4PZIPsvn2h1sArYOhgXQ=" crossorigin="anonymous"></script>
		<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js" integrity="sha256-xNjb53/rY+WmG+4L6tTl9m6PpqknWZvRt0rO1SRnJzw=" crossorigin="anonymous"></script>
		
		<?php
		echo $this->Html->script(array(
			'admin/bootstrap.min', 
			'admin/detect',
			'admin/fastclick', 
			'admin/jquery.slimscroll', 
			'admin/jquery.blockUI', 
			'admin/waves',
			'admin/wow.min',
			'admin/jquery.nicescroll', 
			'admin/jquery.scrollTo.min',
		));

		echo $this->Html->script(array(
			'admin/jquery.core',
			'admin/jquery.app'
		));?>

		<?php echo $this->fetch('scriptBottom'); ?>

	</body>
</html>