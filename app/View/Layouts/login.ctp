<!DOCTYPE html>
<html>
	<head>
		<?php echo $this->Html->charset(); ?>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="Sistema Administrativo">
		<meta name="author" content="Renato Castanheiro">
		<meta name="robots" content="noindex, nofollow" />

		<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css('admin/bootstrap.min');
		echo $this->Html->css('admin/core');
		echo $this->Html->css('admin/components');
		echo $this->Html->css('admin/icons');
		echo $this->Html->css('admin/pages');
		echo $this->Html->css('admin/responsive');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
		?>

		<title><?php echo $this->fetch('title'); ?></title>

		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<?php echo $this->Html->script('admin/modernizr.min');?>
	</head>


	<body>
		<div class="account-pages"></div>
		<div class="clearfix"></div>
		<div class="wrapper-page">
			<div class=" card-box">
				<div class="panel-heading"> 
					<h3 class="text-center"> Azul com Verde</h3>
				</div>
				<div class="panel-body">
					<?php echo $this->fetch('content');?>
				</div>   
			</div>			
		</div>
		
		<script>
			var resizefunc = [];
		</script>
		
		<?php
		echo $this->Html->script(array(
			'admin/jquery.min', 
			'admin/bootstrap.min', 
			'admin/detect.js', 
			'admin/fastclick.js', 
			'admin/jquery.slimscroll', 
			'admin/jquery.blockUI', 
			'admin/waves.js', 
			'admin/wow.min', 
			'admin/jquery.nicescroll', 
			'admin/jquery.scrollTo.min', 
			'../plugins/notifyjs/dist/notify.min',
			'../plugins/notifications/notify-metro',
		));

		echo $this->Html->script(array(
			'admin/jquery.core',
			'admin/jquery.app'
		));

		echo $this->fetch('scriptBottom');
		?>
	</body>
</html>