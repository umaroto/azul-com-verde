<?php
App::uses('AppController', 'Controller');

class ClientesController extends AppController {

	public $paginate = array(
		'limit' => 20,
		'order' => array(
			'Cliente.id' => 'desc'
		)
	);

	public function admin_index() {
		if ($this->request->is('post') || $this->request->is('put')){
			$this->Paginator->settings = array(
				'conditions' => array(
					'OR' => array(
						'Cliente.id' => $this->request->data['Cliente']['id'],
						'Cliente.nome LIKE "%'.$this->request->data['Cliente']['nome'].'%"',
					)
				)
			);
			unset($this->request->data);
		} else {
			$this->Paginator->settings = $this->paginate;
		}

		$clientes = $this->Paginator->paginate('Cliente');

		$this->set(compact('clientes'));
	}

	public function admin_add() {
		if ($this->request->is('post') || $this->request->is('put')){
			
			if(isset($this->request->data['Cliente']['arquivo']) && !empty($this->request->data['Cliente']['arquivo']['name'])){
				$this->request->data['Cliente']['arquivo'] = $this->Cliente->upload($this->request->data['Cliente']['arquivo'], 'images/clientes', 150, 150);
			} else {
				unset($this->request->data['Cliente']['arquivo']);
			}

			if ($this->Cliente->save($this->request->data)) {
				$this->Session->setFlash(__('Cadastro salvo com sucesso.'), 'admin/sucess');
			} else {
				$this->Session->setFlash(__('Erro ao salvar dados.'), 'admin/error');
			}
		} else {
			$this->Session->setFlash(__('Erro Interno'), 'admin/error');
		}

		return $this->redirect(array('action' => 'index'));
	}

	public function admin_search() {
		$this->render(false);
		$cliente = $this->Cliente->find('all', array(
			'conditions' => array(
				'Cliente.nome LIKE "%'.$_GET['term'].'%"',
			),
			'fields' => array('Cliente.id', 'Cliente.nome'),
			'recursive' => -1
		));
		$cliente = Set::classicExtract($cliente, '{n}.Cliente');
		$clientes = array_map(function($cliente) {
			return array(
				'id' => $cliente['id'],
				'value' => $cliente['nome']
			);
		}, $cliente);
		echo json_encode($clientes);
	}

	public function admin_delete(){
		$this->layout = 'ajax';
		$this->render(false);

		if(isset($this->request->data['id']) && !empty($this->request->data['id'])){
			$id = explode('_', $this->request->data['id']);
			$id = end($id);
			$foto = $this->Cliente->findById($id);

			if($this->Cliente->delete($id)){
				if(isset($foto['Cliente']['arquivo']) && !empty($foto['Cliente']['arquivo'])){
					@unlink(WWW_ROOT . 'images/clientes/'.$foto['Cliente']['arquivo']);
				}
				echo $id;
			} else {
				echo 0;
			}
		} else {
			echo 0;
		}
	}
}