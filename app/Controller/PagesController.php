<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link https://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array();

/**
 * Displays a view
 *
 * @return CakeResponse|null
 * @throws ForbiddenException When a directory traversal attempt.
 * @throws NotFoundException When the view file could not be found
 *   or MissingViewException in debug mode.
 */
	public function display() {
		$path = func_get_args();

		$count = count($path);
		if (!$count) {
			return $this->redirect('/');
		}
		if (in_array('..', $path, true) || in_array('.', $path, true)) {
			throw new ForbiddenException();
		}
		$page = $subpage = $title_for_layout = null;

		if (!empty($path[0])) {
			$page = $path[0];
		}
		if (!empty($path[1])) {
			$subpage = $path[1];
		}
		if (!empty($path[$count - 1])) {
			$title_for_layout = Inflector::humanize($path[$count - 1]);
		}
		$this->set(compact('page', 'subpage', 'title_for_layout'));

		try {
			$this->render(implode('/', $path));
		} catch (MissingViewException $e) {
			if (Configure::read('debug')) {
				throw $e;
			}
			throw new NotFoundException();
		}
	}

	public function index(){
		if ($this->request->is('post') || $this->request->is('put')){
			$subject = 'Azul com Verde :: Contato enviado pelo site';
			$htmlContent = '<h1>Contato enviado pelo site em '.date('d/m/Y H:i').'</h1>
			<p>
				Nome: '.$this->request->data['contato_nome'].'<br>
				Email: '.$this->request->data['contato_email'].'<br>
				Assunto: '.$this->request->data['contato_assunto'].'<br>
				Mensagem: '.$this->request->data['contato_mensagem'].'
			</p>';

			$to = 'ceo@azulcomverde.com.br';

			if($this->sendEmail($to, null, null, $subject, null, $htmlContent)){
				$this->Session->setFlash(__('Mensagem enviada com sucesso.'), 'messages/sucess');
			} else {
				$this->Session->setFlash(__('Erro ao enviar mensagem.'), 'messages/error');
			}
		}

		$this->LoadModel('Depoimento');
		$this->LoadModel('Parceiro');
		$this->LoadModel('Cliente');

		$depoimentos = $this->Depoimento->find('all');
		$parceiros = $this->Parceiro->find('all');
		$clientes = $this->Cliente->find('all');

		$this->set(compact('depoimentos', 'parceiros', 'clientes'));
	}

	public function erro404(){
	}

	private function sendEmail($to = null, $from = null, $fromName = null, $subject, $file = null, $htmlContent){

		$from = isset($from) ? $from : 'thehostel@thehostel.online';
		$fromName = isset($fromName) ? $fromName : 'The Hostel';

		$semi_rand = md5(time()); 
		$mime_boundary = "==Multipart_Boundary_x{$semi_rand}x";
		$headers = "From: $fromName"." <".$from.">";
		$headers .= "\nMIME-Version: 1.0\n" . "Content-Type: multipart/mixed;\n" . " boundary=\"{$mime_boundary}\""; 

		$message = "--{$mime_boundary}\n" . "Content-Type: text/html; charset=\"UTF-8\"\n" .
		"Content-Transfer-Encoding: 7bit\n\n" . $htmlContent . "\n\n";
		$message .= "--{$mime_boundary}--";
		$returnpath = "-f" . $from;

		return @mail($to, $subject, $message, $headers, $returnpath); 
	}
}