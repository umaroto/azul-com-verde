<?php
App::uses('AppController', 'Controller');

class NewslettersController extends AppController {

	public $paginate = array(
		'limit' => 9,
		'order' => array(
			'Newsletter.id' => 'desc'
		)
	);

	public function add() {
		if ($this->request->is('post') || $this->request->is('put')){
			try {
				$this->Newsletter->save($this->request->data);
				$this->Session->setFlash(__('Cadastro salvo com sucesso.'), 'messages/sucess');
			} catch(Exception $e) {
				$this->Session->setFlash(__('Você já está registrado.'), 'messages/error');
			}
		} else {
			$this->Session->setFlash(__('Erro Interno'), 'messages/error');
		}

		return $this->redirect($this->referer());
	}

	public function admin_index() {
		if ($this->request->is('post') || $this->request->is('put')){
			$this->Paginator->settings = array(
				'conditions' => array(
					'OR' => array(
						'Newsletter.id' => $this->request->data['Newsletter']['id'],
						'Newsletter.email LIKE "%'.$this->request->data['Newsletter']['email'].'%"',
					)
				)
			);
			unset($this->request->data);
		} else {
			$this->Paginator->settings = $this->paginate;
		}

		$newsletters = $this->Paginator->paginate('Newsletter');
		$this->set(compact('newsletters'));
	}

	public function admin_edit($id = null) {
		$this->layout = 'ajax';
		$this->request->data = $this->Newsletter->find('first', array(
			'conditions' => array(
				'Newsletter.id' => $id,
			),
			'recursive' => -1
		));

		$this->set(compact('newsletters'));
	}

	public function admin_search() {
		$this->render(false);
		$newsletter = $this->Newsletter->find('all', array(
			'conditions' => array(
				'Newsletter.email LIKE "%'.$_GET['term'].'%"',
			),
			'fields' => array('Newsletter.id', 'Newsletter.email'),
			'recursive' => -1
		));
		$newsletter = Set::classicExtract($newsletter, '{n}.Newsletter');
		$newsletters = array_map(function($newsletter) {
			return array(
				'id' => $newsletter['id'],
				'value' => $newsletter['email']
			);
		}, $newsletter);
		echo json_encode($newsletters);
	}

	public function admin_exporta() {
		$this->response->download("newsletter.csv");
		$newsletters = $this->Newsletter->find('all');
		$this->set(compact('newsletters'));
		$this->layout = 'ajax';
		return;
	}
}